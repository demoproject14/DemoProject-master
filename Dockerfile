FROM 60.24.64.107/daocloud/jdk-7
ADD ./target/api-1.0-SNAPSHOT.jar ./api-1.0-SNAPSHOT.jar
ENTRYPOINT ["sh", "-c"]
CMD ["java  -jar ./api-1.0-SNAPSHOT.jar"]
